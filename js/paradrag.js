/*
 * Added "context" to all selectors to fix css issues
 * when using drag & drop on sub-paragraphs
 * and when adding new sub-paragraphs
 */
(function ($, Drupal, drupalSettings) {

    "use strict";
    var idtell = 0;
    var whichpara = 0;

    Drupal.behaviors.paradrag = {
        attach: function attach(context, drupalSettings) {

            if ($('input[id*=confirm-delete-button]').length) {
                $('html, body').animate({
                    scrollTop: $('input[id*=confirm-delete-button]').offset().top - 300
                }, 500);
                return;
            }

            var BgColor = $('<style>.paratell { background-color: ' + drupalSettings.paradrag.color + '}</style>');
            $('html > head').append(BgColor);


            $('.paracollapse', context).contents().unwrap();
            // $('.paralabel').remove();

            $('.paragraph-type-top', context).each(function () {

                if (!$(this, context).parent().parent().parent().hasClass('ajax-new-content')) {

                    // $(this).removeClass();
                    $(this, context).addClass('paratell pt-' + idtell);
                    $(this, context).attr('rel', idtell);

                    var label = $(this, context).parent().find('input[id*=subform-field-title-]').val();
                    if (label === undefined) {
                        label = '';
                    } else {
                        label = ' | ' + label;
                    }

                    var paratext = $(this, context).text();
                    // Temp fix
                    if (paratext.indexOf(' | ') === -1) {
                        $('.paragraph-type-title', this).text(paratext + label);
                    }
                    var others = $(this, context).parent().children('div:not(:first-child)');
                    others.wrapAll('<div class="paracollapse pc-' + idtell + '" />');
                    idtell++;
                } else {
                    console.warn('Has ajax new class');
                }

            });
            $('.paracollapse', context).each(function () {
                if (!$(this, context).find('.ajax-new-content').length) {

                    $(this, context).hide();
                }
            });
            $('.paratell', context).unbind('click');

            $('.paratell', context).click(function () {
                console.log('paratell');
                whichpara = $(this, context).attr('rel');
                $(this, context).toggleClass('opened');
                $('.pc-' + whichpara, context).toggle(500);

            });
        }
    };

})(jQuery, Drupal, drupalSettings);
