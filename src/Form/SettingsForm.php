<?php

namespace Drupal\paradrag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paradrag_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['paradrag.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['color'] = [
      '#title' => $this->t('Select color'),
      '#description' => $this->t('Selected colour will be used in add/edit form.'),
      '#type' => 'color',
      '#default_value' => $this->config('paradrag.settings')->get('color'),
    ];

    $form['include'] = [
      '#title' => $this->t('Include:'),
      '#description' => $this->t('Pick forms with drag & drop functionality.'),
      '#type' => 'checkboxes',
      '#options' => ['edit' => $this->t('Edit Form'), 'add' => $this->t('Add Form')],
      '#default_value' => $this->config('paradrag.settings')->get('include'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('paradrag.settings')
      ->set('color', $form_state->getValue('color'))
      ->set('include', $form_state->getValue('include'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
